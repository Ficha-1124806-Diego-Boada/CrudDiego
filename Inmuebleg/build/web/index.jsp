<%-- 
    Document   : Menu
    Created on : 25-abr-2016, 20:09:43
    Author     : REDP
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="CSS/style.css" rel="stylesheet" type="text/css">
        <link href="CSS/bootstrap.css" rel="stylesheet" media="screen">
        <title>Menu</title>
    </head>
    <body>
        <section class="col-md-12">
        <header>
                <h1 id="titulo">MENU</h1>
        </header>
        </section>
        <section class="col-md-12">
            <h2 id="selec">SELECCIONA UNA OPCION...</h2>
            <section class="col-md-6" id="botones">                
                <button class="but" onclick="location.href = 'cliente/Clientes.jsp'">Clientes</button>
                <button class="but" onclick="location.href = ''">Empleados</button>
                <button class="but" onclick="location.href = ''">Contratos</button>
                <button class="but" onclick="location.href = ''">Facturas</button>
                <button class="but" onclick="location.href = 'inmueble/Inmuebles.jsp'">Inmuebles</button>
                <button class="but" onclick="location.href = ''">Local</button>           
            </section>
        </section>
    </body>    
</html>