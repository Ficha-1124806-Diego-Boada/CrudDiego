<%-- 
    Document   : Buscar
    Created on : 24/04/2016, 07:48:44 PM
    Author     : Usuario
--%>

<%@page import="Modelo.Cliente"%>
<%@page contentType="text/html" pageEncoding="UTF-8" import="java.sql.*"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="../CSS/style.css" rel="stylesheet" type="text/css">
        <link href="../CSS/bootstrap.css" rel="stylesheet" media="screen">
        <title>Buscar Cliente</title>
    </head>
    <body>
        <header>
            <h1 id="titulo">Buscar cliente</h1>
        </header>
        <form action="#" method="post">
            
            <label>Id:</label>
            <input type="number" name="id">
            
            <input type="submit" name="btbuscar">            
        </form>
        
        
        <%! //Declaracion de objetos para la conexion
            ResultSet rs=null; 
            Connection cnx=null;
            Statement st=null;
            
            //Instanciamos un objeto para usar la clase cliente
            Cliente in=new Cliente();
            
            //Declaracion de variables de ayuda
            String url="jdbc:mysql://localhost/inmueble";
            String us="root";
            String ps="";
        %>
        <%
            try{      
        //Generamos codigo de coneccion
        try {
            //
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            
            //Enviamos datos a la clase por medio del objeto creado anteriormente
                in.setId(Integer.parseInt(request.getParameter("id")));
                
                //Usamos DriverManager para realizar conexion
                cnx=DriverManager.getConnection(url,us,ps);
                if(cnx!=null){
                    System.out.println("Coneccion Exitosa");
                    
                    //Llenamos Statement con la conexion
                    st=cnx.createStatement();                   
                    
                    //Realizamos consulta en la base de datos
                    rs=st.executeQuery("select * from cliente where IdCliente="+in.getId());
                }else{
                    System.out.println("Conexion fallida");
                }
            }
        catch(SQLException e){
            System.out.println("Exception:: "+ e);
            }
            
            }catch(Exception ex){
                ex.printStackTrace();
            }
        %>
        <table>
            <tr>
                <td>Id</td>
                <td>Nombre</td>
                <td>Apellido</td>
                <td>Telefono</td>
                <td>EMail</td>
                <td>Direccion</td>
                <td>Estado</td>
            </tr>
            <%while(rs!=null){%>
            <tr>                
                 <%-- llenamos campos de la tabla con los datos de la consulta realizada anteriormente y guardada en el ResultSet--%>
                <td><%=rs.getInt("IdCliente")%></td>
                <td><%=rs.getString("NombreCli")%></td>
                <td><%=rs.getString("ApellidoCli")%></td>
                <td><%=rs.getInt("TelefonoCli")%></td>
                <td><%=rs.getString("EMailCli")%></td>
                <td><%=rs.getString("DireccionCli")%></td>
                <td><%=rs.getInt("FkIdEstado")%></td>
            </tr>
            <%}%>
        </table>
        <br>
        <br>
        <button onclick="location.href= '../index.jsp';" id="regresar">Regresar</button>
    </body>
</html>

