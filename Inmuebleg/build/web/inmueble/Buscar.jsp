<%-- 
    Document   : Buscar
    Created on : 24/04/2016, 07:48:44 PM
    Author     : Usuario
--%>

<%@page import="Modelo.Inmueble"%>
<%@page contentType="text/html" pageEncoding="UTF-8" import="java.sql.*"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="../CSS/style.css" rel="stylesheet" type="text/css">
        <link href="../CSS/bootstrap.css" rel="stylesheet" media="screen">
        <title>Buscar inmueble</title>
    </head>
    <body>
        <header>
            <h1 id="titulo">Buscar inmueble</h1>
        </header>
        <form action="Buscar.jsp" method="post">
            
            <label>Id:</label>
            <input type="number" name="idI">            
            <input type="submit" name="btbuscar">            
        </form>
        
        
        <%! //Declaracion de objetos para la conexion
            ResultSet rs=null; 
            Connection cnx=null;
            Statement st=null;
            
            //Instanciamos un objeto para usar la clase inmueble
            Inmueble in=new Inmueble();
            
            //Declaracion de variables de ayuda
            String url="jdbc:mysql://localhost/inmueble";
            String us="root";
            String ps="";
        %>
        <%
            try{      
        //Generamos codigo de coneccion
        try {
            //Instanciamos en river para realizar la conexion
            Class.forName("com.mysql.jdbc.Driver").newInstance();
               
            //Enviamos datos a la clase por medio del objeto creado anteriormente
            in.setId(Integer.parseInt(request.getParameter("idI")));
                
            //Usamos DriverManager para realizar conexion
                cnx=DriverManager.getConnection(url,us,ps);
                
                if(cnx!=null){
                    System.out.println("Coneccion Exitosa");
                    
                    //Llenamos Statement con la conexion
                    st=cnx.createStatement();                   
                    
                    //Realizamos consulta en la base de datos
                    rs=st.executeQuery("select * from inmueble where IdInmueble="+in.getId());
                }else{
                    System.out.println("Conexion fallida");
                }
            }
        catch(SQLException e){
            System.out.println("Exception:: "+ e);
            }
            
            }catch(Exception ex){
                ex.printStackTrace();
            }
        %>
        <table>
            <tr>
                <td>Id</td>
                <td>Direccion</td>
                <td>Edificio</td>
                <td>Estado</td>
                <td>Cliente</td>
                <td>Empleado</td>
            </tr>
            <%while(rs!=null){%>
            <tr>                
                <%-- llenamos campos de la tabla con los datos de la consulta realizada anteriormente y guardada en el ResultSet--%>
                <td><%=rs.getInt("IdInmueble")%></td>
                <td><%=rs.getString("Direccion")%></td>
                <td><%=rs.getString("Edificio")%></td>
                <td><%=rs.getInt("FkIdEstado")%></td>
                <td><%=rs.getInt("FkIdCliente")%></td>
                <td><%=rs.getInt("FkIdEmpleado")%></td>
            </tr>
            <%}%>
        </table>
        <br>
        <br>
        <button onclick="location.href= '../index.jsp';" id="regresar">Regresar</button>
    </body>
</html>

