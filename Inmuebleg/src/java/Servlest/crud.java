/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlest;

import Modelo.Inmueble;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.*;

/**
 *
 * @author REDP
 */
@WebServlet(name = "crud", urlPatterns = {"/crud"})
public class crud extends HttpServlet {

    //Declaracion de los objetos de coneccion    
    Connection cnx=null;
    Statement st=null;
    ResultSet rs=null;
        
    //Declaracion de variables de ayuda
    String url="jdbc:mysql://localhost/inmueble";
    String us="root";
    String ps="";
    
    //Instanciamos clase
    Inmueble in= new Inmueble();
    
    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        
        try{      
        //Generamos codigo de coneccion
         try {
             //Llamamos driver de conexion
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            
            //if(request.getParameter("btañadir")!=null){
            //Recibe los datos del formulario y los envia a la clase inmueble
                in.setId(Integer.parseInt(request.getParameter("id")));
                in.setDireccion(request.getParameter("direccion"));
                in.setEdificio(request.getParameter("edificio"));
                in.setEstado(Integer.parseInt(request.getParameter("estado")));
                in.setCliente(Integer.parseInt(request.getParameter("cliente")));
                in.setEmpleado(Integer.parseInt(request.getParameter("empleado")));
                
                //hacemos coneccion por medio del DriverManager 
                cnx=DriverManager.getConnection(url,us,ps);
                if(cnx!=null){
                    System.out.println("Coneccion Exitosa");
                    
                    //Llenamos Statement con la conexion
                    st=cnx.createStatement();
                    
                    //Usamos el statement para enviar datos a la base de datos
                    if(!st.execute("insert into inmueble values("+in.getId()+",'"+in.getDireccion()+"','"+in.getEdificio()+"',"+in.getEstado()+","+in.getCliente()+","+in.getEmpleado()+")")) 
                        {
                        System.out.println("Envio de datos exitoso");
                        //Redirecionamos al menu para que el usuariuo realice otra opcion
                        response.sendRedirect("../Inmuebles.jsp");
                        }
                    
                     else{
                            System.out.println("Envio de datos fallido");
                         }
                }else
                    {
                        System.out.println("conexion fallida");
                    }
          }
        catch(SQLException e){
            System.out.println("Exception:: "+ e);
            }
            
            }
    catch(Exception ex){
          ex.printStackTrace();
         }
 }
    

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
