/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlest;

import Modelo.Cliente;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Usuario
 */
@WebServlet(name = "crudCliente", urlPatterns = {"/crudCliente"})
public class crudCliente extends HttpServlet {

    //Declaracion de los objetos de coneccion    
    Connection cnx=null;
    Statement st=null;
    ResultSet rs=null;
        
    //Declaracion de variables de ayuda
    String url="jdbc:mysql://localhost/inmueble";
    String us="root";
    String ps="";
    
    //Instanciamos clase
    Cliente in= new Cliente();
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try{      
        //Generamos codigo de coneccion
        try {
            //instanciamos driver de conexion
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            
            if(request.getParameter("btañadir")!=null){
            //Recibe los datos del formulario y los envia a la clase cliente
                in.setId(Integer.parseInt(request.getParameter("id")));
                in.setNombre(request.getParameter("nombre"));
                in.setApellido(request.getParameter("apellido"));
                in.setTelefono(Integer.parseInt(request.getParameter("telefono")));
                in.setEmail(request.getParameter("email"));
                in.setDireccion(request.getParameter("direccion"));
                in.setEstado(Integer.parseInt(request.getParameter("estado")));
                
                //hacemos coneccion por medio del DrivermManager 
                cnx=DriverManager.getConnection(url,us,ps);
                if(cnx!=null){
                    System.out.println("Coneccion Exitosa");
                    
                    //Llenamos Statement con la conexion
                    st=cnx.createStatement();

                    //Usamos el statement para enviar datos a la base de datos
                    if(!st.execute("insert into cliente values("+in.getId()+",'"+in.getNombre()+"','"+in.getApellido()+"',"+in.getTelefono()+",'"+in.getEmail()+"','"+in.getDireccion()+"',"+in.getEstado()+")"))
                    {
                    //Redirecionamos al menu para que el usuariuo realice otra opcion
                    response.sendRedirect("../../../web/cliente/Clientes.jsp");
                    }
                  else{
                    System.out.println("Conexion fallida");
                }
                }
                
                if(request.getParameter("btmodificar")!=null){
                    
                  //Recibe los datos del formulario y los envia a la clase cliente
                in.setId(Integer.parseInt(request.getParameter("id")));
                in.setNombre(request.getParameter("nombre"));
                in.setApellido(request.getParameter("apellido"));
                in.setTelefono(Integer.parseInt(request.getParameter("telefono")));
                in.setEmail(request.getParameter("email"));
                in.setDireccion(request.getParameter("direccion"));
                in.setEstado(Integer.parseInt(request.getParameter("estado")));    
                
                    cnx=DriverManager.getConnection(url,us,ps);
                    
                if(cnx!=null){
                    
                }
                    System.out.println("Coneccion Exitosa");
                    
                    //Llenamos Statement con la conexion
                    st=cnx.createStatement();
                    int update=st.executeUpdate("update cliente set NombreCli='"+in.getNombre()+"',ApellidoCli='"+in.getApellido()+"',TelefonoCli="+in.getTelefono()+",EMailCli='"+in.getEmail()+"',DireccionCli='"+in.getDireccion()+"',FkIdEstado="+in.getEstado()+" where IdCliente="+in.getId()+"");
                    
                    if(update>0){
                        response.sendRedirect("index.jsp");
                    }

                    //Usamos el statement para enviar datos a la base de datos
                    if(!st.execute("insert into cliente values("+in.getId()+",'"+in.getNombre()+"','"+in.getApellido()+"',"+in.getTelefono()+",'"+in.getEmail()+"','"+in.getDireccion()+"',"+in.getEstado()+")"))
                    {
                    //Redirecionamos al menu para que el usuariuo realice otra opcion
                    response.sendRedirect("../../../web/cliente/Clientes.jsp");
                    }
                  else{
                    System.out.println("Conexion fallida");
                }
                    
                }
        }
        catch(SQLException e){
            System.out.println("Exception:: "+ e);
            }
            
            }catch(Exception ex){
                    ex.printStackTrace();
            }
        }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
