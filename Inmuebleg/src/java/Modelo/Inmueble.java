/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author REDP
 */
public class Inmueble {
    
    private int id;
    private String direccion;
    private String edificio;
    private int estado;
    private int cliente;
    private int empleado;

    public Inmueble() {
    }

    public Inmueble(int id, String direccion, String edificio, int estado, int cliente, int empleado) {
        this.id = id;
        this.direccion = direccion;
        this.edificio = edificio;
        this.estado = estado;
        this.cliente = cliente;
        this.empleado = empleado;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getEdificio() {
        return edificio;
    }

    public void setEdificio(String edificio) {
        this.edificio = edificio;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public int getCliente() {
        return cliente;
    }

    public void setCliente(int cliente) {
        this.cliente = cliente;
    }

    public int getEmpleado() {
        return empleado;
    }

    public void setEmpleado(int empleado) {
        this.empleado = empleado;
    }
    
    
    
    
}
