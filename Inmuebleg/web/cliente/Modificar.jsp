<%-- 
    Document   : Modificar
    Created on : 24/04/2016, 11:59:34 PM
    Author     : Usuario
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="CSS/style.css" rel="stylesheet" type="text/css">
        <link href="CSS/bootstrap.css" rel="stylesheet" media="screen">
        <title>Modificar Cliente</title>
    </head>
    <body>
     <section class="container-fluid">
       
            <section class="col-md-12">
                <header>
                    <h1 id="titulo">MODIFICAR CLIENTE</h1>
                </header>
            <section class="col-md-7">
                <%--Formulario de modificar cliente--%>
       
                <form id="formulario1" method="post" action="../crudCliente">
                    <article class="form-group">
                        <label>Id:</label>
                        <input class="form-control" id="n" name="id" type="text"/>
                    </article>
                    <article class="form-group">
                        <label>Nombres:</label>
                        <input class="form-control" id="a" name="nombre" type="text"/>
                    </article>   
                    <article class="form-group">    
                        <label>Apellidos:</label>
                        <input class="form-control" id="t" name="apellido" type="text"/>
                    </article>
                    <article class="form-group">
                        <label>Telefono:</label>
                        <input class="form-control" id="e" name="telefono" type="text"/>
                    </article>
                    <article class="form-group">
                        <label>EMail:</label>
                        <input class="form-control" id="d" name="email" type="text"/>
                    </article>
                    <article class="form-group">
                        <label>Direccion:</label>
                        <input class="form-control" id="d" name="direccion" type="text"/>
                    </article>
                    <article class="form-group">
                        <label>Estado:</label>
                        <input class="form-control" id="d" name="estado" type="text"/>
                    </article>
                    <center><input type='submit' name="btmodificar" value="Guardar cambios"/></center>
                </form>
            </section>
            </section>
               
                    <button onclick="location.href= 'index.jsp';" id="regresar">Regresar</button>
               
         </section>
    </body>
</html>
