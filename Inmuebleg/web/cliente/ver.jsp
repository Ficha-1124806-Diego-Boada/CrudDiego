<%-- 
    Document   : ver
    Created on : 25/04/2016, 01:34:00 AM
    Author     : Usuario
--%>

<%@page import="Modelo.Inmueble"%>
<%@page contentType="text/html" pageEncoding="UTF-8" import="java.sql.*"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="../CSS/style.css" rel="stylesheet" type="text/css">
        <link href="../CSS/bootstrap.css" rel="stylesheet" media="screen">
        <title>Lista clientes</title>
    </head>
    <body>
        <header>
            <h1 id="titulo">Lista clientes</h1>
        </header>
        <%! 
            ResultSet rs=null; 
            Connection cnx=null;
            Statement st=null;
            Inmueble in=new Inmueble();
            String url="jdbc:mysql://localhost/inmueble";
            String us="root";
            String ps="";
        %>
        <%
            try{      
        //Generamos codigo de coneccion
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
                cnx=DriverManager.getConnection(url,us,ps);
                if(cnx!=null){
                    System.out.println("Coneccion Exitosa");
                    st=cnx.createStatement();                   
                    rs=st.executeQuery("select * from cliente");
                }else{
                    System.out.println("Conexion fallida");
                }
            }
        catch(SQLException e){
            System.out.println("Exception:: "+ e);
            }

            }catch(Exception ex){
                ex.printStackTrace();
            }
        %>
        <table>
            <tr>
                <td>Id</td>
                <td>Nombre</td>
                <td>Apellido</td>
                <td>Telefono</td>
                <td>Email</td>
                <td>Direccion</td>
                <td>Estado</td>
            </tr>
            <%while(rs.next()){%>
            <tr>                
                <td><%=rs.getInt("IdCliente")%></td>
                <td><%=rs.getString("NombreCli")%></td>
                <td><%=rs.getString("Apellidocli")%></td>
                <td><%=rs.getInt("TelefonoCli")%></td>
                <td><%=rs.getString("EMailCli")%></td>
                <td><%=rs.getString("DireccionCli")%></td>
                <td><%=rs.getInt("FkIdEstado")%></td>
            </tr>
            <%}%>
        </table>
        <br>
        <br>
        <button onclick="location.href= '../index.jsp';" id="regresar">Regresar</button>
    </body>
</html>
