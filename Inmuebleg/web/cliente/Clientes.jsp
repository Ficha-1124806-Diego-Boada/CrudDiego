<%-- 
    Document   : index
    Created on : 25-abr-2016, 18:04:42
    Author     : REDP
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="../CSS/style.css" rel="stylesheet" type="text/css">
        <link href="../CSS/bootstrap.css" rel="stylesheet" media="screen">
        <title>Menu Clientes</title>
    </head>
    <body>
        <section class="col-md-12">
        <header>
                <h1 id="titulo">clientes</h1>
        </header>
        </section>
        <section class="col-md-12">
            <h2 id="selec">SELECCIONA UNA OPCION...</h2>
            <section class="col-md-6" id="botones">                
                <button class="but" onclick="location.href = 'Nuevo.jsp'">Agregar Cliente</button>
                <button class="but" onclick="location.href = 'Buscar.jsp'">Buscar Cliente</button>
                <button class="but" onclick="location.href = 'Modificar.jsp'">Modificar Cliente</button>
                <button class="but" onclick="location.href = 'ver.jsp'">Ver Clientes</button><br><br>       
                <button class="but" onclick="location.href = '../index.jsp'">Regresar</button>   
            </section>
        </section>
    </body>    
</html>
