<%--
    Document   : Nuevo
    Created on : 24/03/2016, 09:19:23 PM
    Author     : Usuario
--%>



<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="../CSS/style.css" rel="stylesheet" type="text/css">
        <link href="../CSS/bootstrap.css" rel="stylesheet" media="screen">
        <title>Nuevo Inmueble</title>
    </head>
    <body>
     <section class="container-fluid">
       
            <section class="col-md-12">
                <header>
                    <h1 id="titulo">añadir inmueble</h1>
                </header>
            <section class="col-md-7">
                <%--Formulario de añadir inmueble--%>
                
                <form id="formulario1" method="post" action="../crud">
                    
                    <article class="form-group">
                        <label>Id:</label>
                        <input class="form-control" id="n" name="id" type="text"/>
                    </article>
                    <article class="form-group">
                        <label>Direccion:</label>
                        <input class="form-control" id="a" name="direccion" type="text"/>
                    </article>   
                    <article class="form-group">    
                        <label>Edificio:</label>
                        <input class="form-control" id="t" name="edificio" type="text"/>
                    </article>
                    <article class="form-group">
                        <label>Codigo Estado:</label>
                        <input class="form-control" id="e" name="estado" type="text"/>
                    </article>
                    <article class="form-group">
                        <label>Id Cliente:</label>
                        <input class="form-control" id="d" name="cliente" type="text"/>
                    </article>
                    <article class="form-group">
                        <label>Id Empleado:</label>
                        <input class="form-control" id="em" name="empleado" type="text"/>
                    </article>
                    <input type='submit' name="btañadir" value="Añadir Contacto"/>
                </form>
            </section>
                <section class="col-md-5" id="icono">
                    <img src="imagenes/tel.png" alt="icono telefono">
                    <button onclick="location.href= '../index.jsp';" id="regresar">Regresar</button>
                </section>
         </section>
        </section>
    </body>
</html>
