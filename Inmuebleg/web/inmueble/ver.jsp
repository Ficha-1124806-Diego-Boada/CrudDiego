<%-- 
    Document   : ver
    Created on : 25/04/2016, 01:34:00 AM
    Author     : Usuario
--%>

<%@page import="Modelo.Inmueble"%>
<%@page contentType="text/html" pageEncoding="UTF-8" import="java.sql.*"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="../CSS/style.css" rel="stylesheet" type="text/css">
        <link href="../CSS/bootstrap.css" rel="stylesheet" media="screen">
        <title>Lista inmuebles</title>
    </head>
    <body>
        <header>
            <h1 id="titulo">Lista inmuebles</h1>
        </header>
        
        <%! //Declaracion de objetos para la conexion
            ResultSet rs=null; 
            Connection cnx=null;
            Statement st=null;
            
            //Instanciamos un objeto para usar la clase inmueble
            Inmueble in=new Inmueble();
            
            //Declaracion de variables de ayuda
            String url="jdbc:mysql://localhost/inmueble";
            String us="root";
            String ps="";
        %>
        <%
            try{      
        //Generamos codigo de coneccion
        try {
            //Instanciamos driver para la conexion
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            
            //Realizamos la coneccion por medio del DriverManager
                cnx=DriverManager.getConnection(url,us,ps);
                if(cnx!=null){
                    System.out.println("Coneccion Exitosa");
                    
                    //Llenamos Statement con la conexion
                    st=cnx.createStatement();   
                    
                    //Realizamos consulta desde la base de datos y la guardamos en el ResultSet
                    rs=st.executeQuery("select * from inmueble");
                }else{
                    System.out.println("Conexion fallida");
                }
            }
        catch(SQLException e){
            System.out.println("Exception:: "+ e);
            }

            }catch(Exception ex){
                ex.printStackTrace();
            }
        %>
        <table>
            <tr>
                <td>Id</td>
                <td>Direccion</td>
                <td>Edificio</td>
                <td>Estado</td>
                <td>Cliente</td>
                <td>Empleado</td>
            </tr>
            <% while(rs.next()){%>
            <tr>                
                <%-- llenamos campos de la tabla con los datos de la consulta realizada anteriormente y guardada en el ResultSet--%>
                <td><%=rs.getInt("IdInmueble")%></td>
                <td><%=rs.getString("Direccion")%></td>
                <td><%=rs.getString("Edificio")%></td>
                <td><%=rs.getInt("FkIdEstado")%></td>
                <td><%=rs.getInt("FkIdCliente")%></td>
                <td><%=rs.getInt("FkIdEmpleado")%></td>
            </tr>
            <%}%>
        </table>
        <br>
        <br>
        <button onclick="location.href= '../index.jsp';" id="regresar">Regresar</button>
    </body>
</html>
