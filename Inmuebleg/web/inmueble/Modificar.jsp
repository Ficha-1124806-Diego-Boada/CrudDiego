<%-- 
    Document   : Modificar
    Created on : 24/04/2016, 11:59:34 PM
    Author     : Usuario
--%>

<%@page import="j.Contacto"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="CSS/style.css" rel="stylesheet" type="text/css">
        <link href="CSS/bootstrap.css" rel="stylesheet" media="screen">
        <title>Modificar Contacto</title>
    </head>
    <body>
     <section class="container-fluid">
       
            <section class="col-md-12">
                <header>
                    <h1 id="titulo">MODIFICAR CONTACTO</h1>
                </header>
            <section class="col-md-7">
                <%--Formulario de añadir contacto--%>
                <form id="formulario3" method="post" action="#">
                    <article class="form-group">
                        <label>Nombre contacto:</label>
                        <input class="form-control" id="n" name="nombre" type="text"/>
                        <center><input type='submit' id="buscar" value="Modificar Contacto"/></center>
                    </article>
                </form>
                <section id="borde">
                <form id="formulario1" method="post" action="#">
                    <article class="form-group">
                        <label>Nombre:</label>
                        <input class="form-control" id="n" name="nombre" type="text"/>
                    </article>
                    <article class="form-group">
                        <label>Apellido:</label>
                        <input class="form-control" id="a" name="apellido" type="text"/>
                    </article>   
                    <article class="form-group">    
                        <label>Telefono:</label>
                        <input class="form-control" id="t" name="telefono" type="text"/>
                    </article>
                    <article class="form-group">
                        <label>Email:</label>
                        <input class="form-control" id="e" name="email" type="text"/>
                    </article>
                    <article class="form-group">
                        <label>Direccion:</label>
                        <input class="form-control" id="d" name="direccion" type="text"/>
                    </article>
                    <center><input type='submit' id="añadir" value="Guardar cambios"/></center>
                </form>
            </section>
            </section>
                <section class="col-md-5" id="icono">
                    <img src="imagenes/ed.png" alt="icono telefono" id="lupa">
                    <button onclick="location.href= 'index.jsp';" id="regresar">Regresar</button>
                </section>
         </section>
        <%! Contacto c=new Contacto(); %>       
        <%
                try{
                   if(request.getParameter("añadir")!=null){
                      c.set_nombre(request.getParameter("nombre"));
                      c.set_apellido(request.getParameter("apellido"));
                      c.set_telefono(request.getParameter("telefono"));
                      c.set_email(request.getParameter("email"));
                      c.set_direccion(request.getParameter("direccion"));
                      c.EscribirFichero();
                   } 
                    
                    }catch(Exception e){
                       e.printStackTrace();
                    }
            %>
        </section>
    </body>
</html>
